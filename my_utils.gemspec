Gem::Specification.new do |s|
    s.name        = 'my_utils'
    s.version     = '0.0.0'
    s.date        = '2010-04-28'
    s.summary     = "Hola!"
    s.description = "A simple hello world gem"
    s.authors     = ["Nick Quaranto"]
    s.email       = 'nick@quaran.to'
    s.files       = Dir["lib/*"] + Dir["lib/*/*"]
    s.homepage    = 'http://rubygems.org/gems/my_utils'
end
