module Strutils

	def self.reg_iter text, s, e
		rstart = 0; rend = 0
		rst_arry = []

		while true
		    rstart = text.index(s, rend)
		    break if rstart == nil

		    rend = text.index(e, rstart)
		    len = text.match(e, rstart)[0].length
		    rend = rend + len - 1
		    rst_arry.push(text[rstart..rend])
		    rstart = rend
		end

		rst_arry
    end	

    def self.strcmp a, b
		return 0 if a == nil || b == nil 

		a = a.strip; b = b.strip

		return 0 if a == "" || b == ""

		len = a.length > b.length ? a.length : b.length
		dp = Array.new(a.length) {Array.new(b.length, 0)}

		for i in 0..a.length-1
		    for j in 0..b.length-1
			(dp[i][j] = (a[0] == b[0]? 1 : 0); next) if i == 0 and j == 0
			(dp[i][j] = (dp[i][j-1] == 1 || a[0] == b[j]? 1 : 0); next) if i == 0
			(dp[i][j] = (dp[i-1][j] == 1 || a[i] == b[0]? 1 : 0); next) if j == 0
			
			dp[i][j] = (dp[i-1][j-1] + (a[i] == b[j]? 1 : 0))
			dp[i][j] = dp[i-1][j] if dp[i][j] < dp[i-1][j]
			dp[i][j] = dp[i][j-1] if dp[i][j] < dp[i][j-1]
		    end 
		end 
		
		return dp[a.length-1][b.length-1] / (len * 1.0)
    end	
end
