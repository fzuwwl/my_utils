#encoding:utf-8

require 'digest/md5'
require 'httpclient'
require 'net/http'
require 'json'
require 'uri'

module Toputils

    TOP_URL = 'http://gw.api.taobao.com/router/rest?'
	STREAM_URL = "http://stream.api.taobao.com/stream" 
	SB_STREAM_URL = "http://stream.api.tbsandbox.com/stream"

    def self.top_get(app_key, app_secret, opts = {})
		params = {
			app_key: app_key,
			format: 'json', 
			v: '2.0', 
			timestamp: (Time.now).strftime("%Y-%m-%d %H:%M:%S"),  
			sign_method: 'md5'
	    }
		params.merge!(opts)

		sign_str = app_secret + params.sort.flatten.join + app_secret

		params[:sign] = Digest::MD5.hexdigest(sign_str).upcase

		params_str = ''
		params.each {
		    |k, v| params_str << k.to_s<<'='<<URI.escape(v.to_s).to_s<<'&'
		}
		target = TOP_URL + params_str

		res = Net::HTTP.get_response(URI.parse(target))
		res.body
    end

	def self.top_stream(options = {})
	    params={
	      "sign_method" => "md5",
	      "app_key" => options[:app_key],
	      "timestamp" => Time.now.strftime("%Y-%m-%d %H:%M:%S")
	    }.sort_by{ |k| k.to_s }
	      
	    post_params = {}
	    params.map { |key, value| post_params[key] = value }
	    post_params["sign"] = Digest::MD5.hexdigest(options[:app_secret] + params.sort_by { |k,v| k.to_s }.flatten.join + options[:app_secret]).upcase
	    c = HTTPClient.new

	    conn = c.post_async(options[:app_secret].start_with?("sandbox")? SB_STREAM_URL: STREAM_URL, post_params)
	    res = conn.pop
	    msg = ""

	    while str = res.content.read(1)
	      unless(str == "\n")
	        msg = msg+str
	        next
	      end
	      result_json = JSON.parse(msg)
	      
	      #TODO处理消息的代码，这里需要改成异步的，消息量大的时候，会产生消息积压
	      code=result_json["packet"]["code"]
	      case code
	      when 200
	        puts "服务器连接成功，连接的服务器为#{result_json["packet"]["msg"]}"
	        #$msg_log.info("服务器连接成功，连接的服务器为#{result_json["packet"]["msg"]}")
	      when 201
	        puts "接收到心跳包"
	        #$hb_log.info("接收到心跳包")
	      when 202
	        puts "有业务消息推送到，消息为#{result_json["packet"]["msg"]}"
	        #$msg_log.info("有业务消息推送到，消息为#{result_json["packet"]["msg"]}")
	        info = result_json['packet']['msg']['notify_trade']
	        yield info
	        #comment(info) if info != nil and info['status'] == 'TradeRated'
	        # TODO
	        #TaobaoTrade.stream_trade_msg result_json["packet"]["msg"]
	      when 203
	        puts "有消息丢失，丢失的消息#{result_json["packet"]["msg"]}"
	        #$msg_log.info("有消息丢失，丢失的消息#{result_json["packet"]["msg"]}")
	      when 101
	        puts "需要重新连接，已到达最长的服务器连接时间"
	        #$msg_log.info("需要重新连接，已到达最长的服务器连接时间")
	      when 102
	        puts "服务器正在升级，需要#{result_json["packet"]["msg"]}秒后进行重连"
	        #$msg_log.info("服务器正在升级，需要#{result_json["packet"]["msg"]}秒后进行重连")
	      when 103
	        puts "服务器主动断开连接，需要#{result_json["packet"]["msg"]}秒后进行重连"
	        #$msg_log.info("服务器主动断开连接，需要#{result_json["packet"]["msg"]}秒后进行重连")
	      when 104
	        puts "发起了新连接，服务器将要断开此连接，以新连接处理消息请求"
	        #$msg_log.info("发起了新连接，服务器将要断开此连接，以新连接处理消息请求")
	      when 105
	        puts "产生大量的消息积压，服务器需要断开连接"
	      else
	        puts "服务器返回了未知的应答码：code=#{result_json["packet"]["code"]},msg=#{result_json["packet"]["msg"]}"
	        #$msg_log.info("服务器返回了未知的应答码：code=#{result_json["packet"]["code"]},msg=#{result_json["packet"]["msg"]}")
	      end

	      yield result_json

	      msg=""
	    end
	end 
end
